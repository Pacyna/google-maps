const mapContainer = document.querySelector('#map');
const searchInput = document.querySelector('.search-input');
const searchButton = document.querySelector('.search-button');
const defaultMapObject = {
    center: { lat: 52.379189, lng: 4.899431 },
    zoom: 8
};
const geoCoder = new google.maps.Geocoder();
const map = new google.maps.Map(mapContainer, defaultMapObject);

const initMap = (location) => {
    const latitude = location.lat();
    const longtitude = location.lng();
    const pinObject = {
        position: location,
        map
    };
    map.setCenter({ lat: latitude, lng: longtitude });
    const marker = new google.maps.Marker(pinObject);
};

const generateNewCoordsFromStringValue = (value) => {
    geoCoder.geocode({ address: value }, (result, status) => {
        if (status === google.maps.GeocoderStatus.OK) {
            const { location } = result[0].geometry;
            initMap(location);
        } else {
            window.alert(status);
        }
    });
};

searchInput.addEventListener('keyup', (event) => {
    if (event.key === 'Enter') {
        generateNewCoordsFromStringValue(searchInput.value);
    }
});

searchButton.addEventListener('click', () => {
    generateNewCoordsFromStringValue(searchInput.value);
});
